CREATE USER 'opencart'@'%';
CREATE DATABASE IF NOT EXISTS opencart;
GRANT ALL ON opencart.* TO 'opencart'@'%' IDENTIFIED BY 'opencartpw';

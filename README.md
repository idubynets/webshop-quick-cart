# Quick cart demo #

## Setup instructions ##

1. Setup docker
2. build the container

        docker-compose build

3. run it

        docker-compose up

The web site will be available on 8019 port, so navigate:

        http://localhost:8019